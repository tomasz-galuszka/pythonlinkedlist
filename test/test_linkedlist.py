import unittest

from linkedlist.linkedlist import LinkedList, OperationNotAllowedException


class LinkedListTest(unittest.TestCase):
    def setUp(self):
        self.myList = LinkedList()

    def test_EmptyListCheckList(self):
        self.assertEqual(0, self.myList.get_size())

    def test_EmptyListRemoveOneCheckList(self):
        with self.assertRaises(OperationNotAllowedException) as context:
            self.myList.remove(0)

        self.assertTrue('List is empty' in context.exception)

    def test_addOneCheckList(self):
        self.myList.add("First Item")

        self.assertEqual(1, self.myList.get_size())
        self.assertEqual("First Item", self.myList.get(0))

    def test_addTwoCheckList(self):
        self.myList.add("First")
        self.myList.add("Second")

        self.assertEqual(2, self.myList.get_size())
        self.assertEqual("First", self.myList.get(0))
        self.assertEqual("Second", self.myList.get(1))

    def test_addThreeCheckList(self):
        self.myList.add("First")
        self.myList.add("Second")
        self.myList.add("Third")

        self.assertEqual(3, self.myList.get_size())
        self.assertEqual("First", self.myList.get(0))
        self.assertEqual("Second", self.myList.get(1))
        self.assertEqual("Third", self.myList.get(2))

    def test_add200CheckList(self):
        for i in range(0, 200):
            self.myList.add(i)

        self.assertEqual(200, self.myList.get_size())
        for i in range(0, 200):
            self.assertEqual(i, self.myList.get(i))

    def testAddOneRemoveCheckList(self):
        self.myList.add(10)

        self.myList.remove(0)

        self.assertEqual(0, self.myList.get_size())

    def testAddTwoRemoveAllCheckListSize(self):
        self.myList.add(10)
        self.myList.add(20)

        self.myList.remove(0)
        self.myList.remove(1)

        self.assertEqual(0, self.myList.get_size())

    def testAddTwoAndRemoveLastCheckList(self):
        self.myList.add(10)
        self.myList.add(20)

        self.myList.remove(1)

        self.assertEqual(1, self.myList.get_size())
        self.assertEqual(10, self.myList.get(0))

    def testAddTwoAndRemoveFirstCheckList(self):
        self.myList.add(10)
        self.myList.add(20)

        self.myList.remove(0)

        self.assertEqual(1, self.myList.get_size())
        self.assertEqual(20, self.myList.get(0))

    def testAddThreeRemoveAllCheckListSize(self):
        self.myList.add(10)
        self.myList.add(20)
        self.myList.add(30)

        self.myList.remove(0)
        self.myList.remove(1)
        self.myList.remove(2)

        self.assertEqual(0, self.myList.get_size())

    def testAddThreeAndRemoveLastCheckList(self):
        self.myList.add(10)
        self.myList.add(20)
        self.myList.add(30)

        self.myList.remove(2)

        self.assertEqual(2, self.myList.get_size())
        self.assertEqual(10, self.myList.get(0))
        self.assertEqual(20, self.myList.get(1))

    def testAddThreeAndRemoveFirstCheckList(self):
        self.myList.add(10)
        self.myList.add(20)
        self.myList.add(30)

        self.myList.remove(0)

        self.assertEqual(2, self.myList.get_size())
        self.assertEqual(20, self.myList.get(0))
        self.assertEqual(30, self.myList.get(1))

    def testAddThreeAndRemoveMiddleCheckList(self):
        self.myList.add(10)
        self.myList.add(20)
        self.myList.add(30)

        self.myList.remove(1)

        self.assertEqual(2, self.myList.get_size())
        self.assertEqual(10, self.myList.get(0))
        self.assertEqual(30, self.myList.get(1))

    def test_add200RemoveAllCheckList(self):
        for i in range(0, 200):
            self.myList.add(i)

        for i in range(0, 200):
            self.myList.remove(i)

        self.assertEqual(0, self.myList.get_size())

    def test_add200RemoveLast100CheckList(self):
        for i in range(0, 200):
            self.myList.add(i)

        for i in range(100, 200):
            self.myList.remove(i)

        self.assertEqual(100, self.myList.get_size())

        for i in range(0, self.myList.get_size()):
            self.assertEqual(i, self.myList.get(i))

    def test_add200RemoveFirst100CheckList(self):
        for i in range(0, 200):
            self.myList.add(i)

        for i in range(0, 100):
            self.myList.remove(0)

        self.assertEqual(100, self.myList.get_size())

        for i in range(0, self.myList.get_size()):
            self.assertEqual(100 + i, self.myList.get(i))

    def test_add200RemoveMiddle100CheckList(self):
        for i in range(0, 200):
            self.myList.add(i)

        for i in range(50, 150):
            self.myList.remove(50)

        self.assertEqual(100, self.myList.get_size())

        for i in range(0, 50):
            self.assertEqual(i, self.myList.get(i))
            self.assertEqual(i + 150, self.myList.get(i + 50))




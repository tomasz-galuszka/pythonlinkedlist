class Element(object):
    def __init__(self, value=None, prev_ref=None):
        self.value = value
        self.prev_ref = prev_ref

    def has_next(self):
        return self.prev_ref is not None

    def get_next(self):
        return self.prev_ref


class OperationNotAllowedException(Exception):
    def __init__(self, message):
        super(Exception, self).__init__(message)


class LinkedList(object):
    REMOVE_HEAD_CMD = 'remove_head'
    REMOVE_AND_UPDATE_CMD = 'remove_and_update'

    def __init__(self):
        self.head = None
        self.size = 0
        self.dispatch = {
            self.REMOVE_HEAD_CMD: self.__remove_head,
            self.REMOVE_AND_UPDATE_CMD: self.__remove_and_update_reference
        }

    def add(self, value):
        new_head = Element(value)
        new_head.prev_ref = self.head

        self.head = new_head

        self.size += 1

    def remove(self, index):
        self.__check_remove_allowed()

        command = self.REMOVE_HEAD_CMD

        prev_element = self.head
        current_head = None

        for i in range(0, (self.size - 1) - index):
            current_head = prev_element
            prev_element = prev_element.prev_ref
            command = self.REMOVE_AND_UPDATE_CMD

        self.dispatch[command](prev_element, current_head)

    def get(self, index):
        item = self.head

        for i in range(0, (self.size - 1) - index):
            item = item.prev_ref

        return item.value

    def get_size(self):
        return self.size

    def __remove_head(self, item, current_head=None):
        self.head = item.prev_ref
        self.size -= 1

    def __check_remove_allowed(self):
        try:
            1 / self.size
        except Exception:
            raise OperationNotAllowedException("List is empty")

    def __remove_and_update_reference(self, prev_item, current_head):
        prev_element_to_join = prev_item.prev_ref
        current_head.prev_ref = prev_element_to_join

        self.size -= 1
